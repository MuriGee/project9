//
//  Petition.swift
//  Project7
//
//  Created by Muri Gumbodete on 22/03/2022.
//

import Foundation

struct Petition: Codable {
    var title: String
    var body: String
    var signatureCount: Int
}
